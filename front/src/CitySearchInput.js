import React from 'react';
import { AutoComplete } from 'antd';

export default function CitySearchInput(props) {
  const options = props.cities.map(c => (
    <AutoComplete.Option
      key={c.id}
      value={c.id.toString()}
    >
      {c.name}
    </AutoComplete.Option>
  ));

  const filterOption = (inputValue, option) =>
    option.props.children.toLowerCase().startsWith(inputValue.toLowerCase());

  return (
    <AutoComplete
      dataSource={options}
      placeholder="Search city..."
      filterOption={filterOption}
      onSelect={props.onCitySelect}
    />
  );
}

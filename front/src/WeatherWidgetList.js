import React from 'react';
import { Card, Col, Row } from 'antd';
import CitySearchInput from './CitySearchInput';
import WeatherWidget from './WeatherWidget';
import 'antd/dist/antd.css';

export default function WeatherWidgetList(props) {
  const widgets = props.widgets.map(w => (
    <WeatherWidget
      widget={w}
      deleteWidget={props.deleteWidget}
      refreshWidget={props.refreshWidget}
      key={w.city.id}
    />
  ));

  const widgetsInRow = 3;

  const widgetChunks = widgets
    .map((_, i) => i % widgetsInRow === 0 ? widgets.slice(i, i + widgetsInRow) : undefined)
    .filter(Boolean);

  const widgetRows = widgetChunks.map(widgets => {
    const widgetCols = widgets.map(w => <Col span={8} key={w.key}>{w}</Col>);

    return (
      <Row
        type="flex"
        justify="start"
        align="middle"
        key={widgets[0].key}
        gutter={8}
        style={{ marginBottom: 8 }}
      >
        {widgetCols}
      </Row>
    );
  });

  return (
    <div style={{ padding: '30px' }}>
      <Row
        type="flex"
        justify="center"
        align="middle"
        style={{ marginBottom: 8 }}
        gutter={8}
      >
        <Col span={12}>
          <Card title="Weather dashboard" bordered={false}>
            <CitySearchInput
              cities={props.cities}
              onCitySelect={props.onCitySelect}
            />
          </Card>
        </Col>
      </Row>
      <Row
        type="flex"
        justify="center"
        align="middle"
        style={{ marginBottom: 8 }}
        gutter={8}
      >
        <Col span={12}>
          {widgetRows}
        </Col>
      </Row>
    </div>
  );
};

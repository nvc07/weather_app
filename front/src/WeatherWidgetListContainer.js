import React from 'react';
import axios from 'axios';
import WeatherWidgetList from './WeatherWidgetList';

export default class WeatherWidgetListContainer extends React.Component {
  state = {
    widgets: [],
    cities: [],
  };

  async getWidgets() {
    const widgets = await axios.get('/api/widgets').then(res => res.data);
    this.setState({ widgets });
  }

  async getCities() {
    const cities = await axios.get('/api/cities').then(res => res.data);
    this.setState({ cities });
  }

  addWidget(cityId) {
    axios.post('/api/widgets', { cityId }).then(_ => this.getWidgets());
  }

  async refreshWidget(id) {
    const refreshedWidget = await axios.get(`/api/widgets/${id}`).then(res => res.data);
    let widgets = this.state.widgets.map(w => Object.assign({}, w));
    Object.assign(widgets.find(w => w.city.id === id), refreshedWidget);
    this.setState({ widgets });
  }

  async deleteWidget(id) {
    await axios.delete(`/api/widgets/${id}`);
    this.getWidgets();
  }

  onCitySelect(value, option) {
    this.addWidget(value);
  }

  componentDidMount() {
    this.getCities();
    this.getWidgets();
  }

  render() {
    return (
      <WeatherWidgetList
        cities={this.state.cities}
        widgets={this.state.widgets}
        onCitySelect={(value, option) => this.onCitySelect(value, option)}
        deleteWidget={(id) => this.deleteWidget(id)}
        refreshWidget={(id) => this.refreshWidget(id)}
      />
    );
  }
}

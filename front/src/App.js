import React, { Component } from 'react';
import WeatherWidgetListContainer from './WeatherWidgetListContainer';

class App extends Component {
  render() {
    return (
      <div style={{ background: '#ECECEC', height: '100vh' }}>
        <WeatherWidgetListContainer/>
      </div>
    );
  }
}

export default App;

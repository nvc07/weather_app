import React from 'react';
import { Button, Card } from 'antd';

export default function WeatherWidget(props) {
  const buttons = (
    <Button.Group size='small'>
      <Button
        icon="redo"
        onClick={() => props.refreshWidget(props.widget.city.id)}
      />
      <Button
        icon="close-circle"
        onClick={() => props.deleteWidget(props.widget.city.id)}
      />
    </Button.Group>
  );

  const title = `${props.widget.city.name}: ${props.widget.data.temperature} °C`;

  const lastUpdateTime = (date) =>
    `${date.getHours()}:${date.getMinutes().toString().padStart(2, '0')}`;

  return (
    <Card
      bordered={false}
      title={title}
      extra={buttons}
    >
      Cloud cover: {props.widget.data.cloudPercentage}%<br/>
      Precipitation: {props.widget.data.rainAmount}<br/>
      Last update: {lastUpdateTime(new Date(props.widget.updatedAt))}
    </Card>
  );
}

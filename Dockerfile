FROM node:9-alpine

ENV NODE_ENV production

WORKDIR /root/code

COPY back/package.json .
RUN yarn --pure-lockfile --prod

COPY back/src src
COPY back/weather_api weather_api
COPY front/build front

CMD yarn run prod

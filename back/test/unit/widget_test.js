test = require('blue-tape');
Widget = require('./../../src/widget');

const getCity = (id) => ({
  id,
  name: `test city ${id}`
});

const getWidget = (id) => ({
  city: getCity(id)
});

test('Widget.getAll should return all widgets in the db', async t => {
  const emptyDbWidgets = await Widget([], undefined).getAll();
  t.equal(emptyDbWidgets.length, 0);

  const widget = getWidget(1);
  const weatherApi = {
    getWeather: () => undefined
  }
  const nonEmptyDbWidgets = await Widget([widget], weatherApi).getAll();
  t.equal(nonEmptyDbWidgets.length, 1);
});

test('Widget.get should return a widget with the given id', async t => {
  const db = [getWidget(1), getWidget(2)];
  const weatherApi = {
    getWeather: () => undefined
  }
  const id = 1;
  const widget = await Widget(db, weatherApi).get(id);
  t.equal(widget.city.id, id);
});

test('Widget.get should return undefined if a widget with the given id does not exist', async t => {
  const id = 1;
  const db = [getWidget(id)];
  const widget = await Widget(db, undefined).get(id + 1);
  t.equal(widget, undefined);
});

test(`Widget.create should add a new widget to the db if a city with the given id exists`, async t => {
  const db = [];
  const id = 1;
  const city = getCity(id);
  const weatherApi = {
    getCity: (id) => [city].find(c => c.id === id) ? city : undefined
  };
  const success = await Widget(db, weatherApi).create(id);
  t.equal(success, true);
  t.equal(db.length, 1);
  t.equal(db[0].city.id, id);
});

test(`Widget.create should not add a new widget if a city with the given id does not exist`, async t => {
  const db = [];
  const id = 1;
  const weatherApi = {
    getCity: (id) => undefined
  };
  const success = await Widget(db, weatherApi).create(id);
  t.equal(success, false);
  t.equal(db.length, 0);
});

test(`Widget.delete should delete a widget with the given id`, async t => {
  const id = 1;
  const db = [getWidget(id)]
  const success = await Widget(db, undefined).delete(id);
  t.equal(success, true);
  t.equal(db.length, 0);
});

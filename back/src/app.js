require('dotenv').config();
const cookieSession = require('cookie-session');
const express = require('express');
const morgan = require('morgan');
const config = require('./config.js');
const weatherApiAdapter = require('./weatherApiAdapter')(config.weatherApi);
const controllers = require('./controllers')(weatherApiAdapter);
const routes = require('./routes')(controllers);
const app = express();

app.disable('x-powered-by');
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cookieSession(config.cookies));

if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('tiny'));
  app.listen(config.port, () => console.log(`listening on port ${config.port}`));
}

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('front'));
  app.get('/', (_, res) => res.sendFile('index.html'));
}

app.use('/api', routes);

const mcache = require('memory-cache');

module.exports = (duration, key, fn) => async (...args) => {
  const cached = mcache.get(key);
  if (cached) return cached;
  const value = await fn(...args);
  mcache.put(key, value, duration * 1000);
  return value;
}

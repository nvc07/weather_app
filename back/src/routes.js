const express = require('express');

module.exports = (controllers) => {
  const router = express.Router();
  const cacheDuration = 12 * 60 * 60;

  router.use((req, res, next) => {
    req.session.widgets = req.session.widgets || [];
    next();
  });

  router.get('/cities', controllers.cities.getAll);

  router.get('/widgets', controllers.widgets.getAll);
  router.get('/widgets/:id', controllers.widgets.get);
  router.post('/widgets', controllers.widgets.post);
  router.delete('/widgets/:id', controllers.widgets.delete);

  router.get('*', (_, res) => res.sendStatus(404));

  router.use((err, req, res, next) => {
    console.log(err);
    res.status(500).send('Internal error');
  });

  return router;
}

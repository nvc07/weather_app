const axios = require('axios');
const cache = require('./cache');

module.exports = ({ apiUrl, apiKey }) => {
  const api = axios.create({
    baseURL: apiUrl,
    headers: {
      authorization: apiKey
    }
  });

  const query = (method, endpoint) => api[method](endpoint).then(res => res.data);

  const cacheDuration = 12 * 60 * 60;
  const getCities = cache(cacheDuration, '__express__city-list', () => query('get', '/city-list'));
  const getCity = (id) => getCities().then(cities => cities.find(c => c.id === id));
  const getWeather = (cityId) => query('get', `/weather/${cityId}`);

  return { getCities, getCity, getWeather };
};

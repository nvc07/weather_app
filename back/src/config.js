const config = {
  port: process.env.PORT || 9299,
  cookies: {
    keys: [
      process.env.COOKIE_SIGNING_KEY,
      process.env.COOKIE_VERIFICATION_KEY
    ],
    maxAge: 2**31 - 1
  },
  weatherApi: {
    apiUrl: process.env.WEATHER_API_URL,
    apiKey: process.env.WEATHER_API_KEY
  }
};

module.exports = config;

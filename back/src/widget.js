module.exports = (widgets, weatherApi) => {
  const widgetExists = (id) => widgets.some(w => w.city.id === id);

  return {
    create: async (id) => {
      if (widgetExists(id)) return false;
      const city = await weatherApi.getCity(id);
      if (!city) return false;
      const widget = {
        city,
        data: undefined,
        updatedAt: undefined
      }
      widgets.push(widget);
      return true;
    },

    get: async (id) => {
      if (!widgetExists(id)) return undefined;
      let widget = widgets.find(w => w.city.id === id);
      widget.data = await weatherApi.getWeather(widget.city.id);
      widget.updatedAt = Date.now();
      return widget;
    },

    getAll: async () => {
      const cityIds = widgets.map(w => w.city.id);
      const data = await Promise.all(cityIds.map(id => weatherApi.getWeather(id)));
      const updatedAt = Date.now();
      widgets.map((w, index) => {
        w.data = data[index];
        w.updatedAt = updatedAt;
      })
      return widgets;
    },

    delete: (id) => {
      const index = widgets.findIndex(w => w.city.id === id);
      widgets.splice(index, 1);
      return true;
    }
  };
};

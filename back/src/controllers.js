const express = require('express');
const Widget = require('./widget');

module.exports = (weatherApi) => {
  const cities = {
    getAll: async (req, res) => {
      res.json(await weatherApi.getCities());
    }
  };

  const widgets = {
    getAll: async (req, res) => {
      const widgets = await Widget(req.session.widgets, weatherApi).getAll();
      res.json(widgets);
    },

    get: async (req, res) => {
      const cityId = parseInt(req.params.id);
      const widget = await Widget(req.session.widgets, weatherApi).get(cityId);
      if (!widget) return res.sendStatus(404);
      res.json(widget);
    },

    post: async (req, res) => {
      const cityId = parseInt(req.body.cityId)
      const success = await Widget(req.session.widgets, weatherApi).create(cityId);
      const httpCode = success ? 204 : 400;
      res.sendStatus(httpCode);
    },

    delete: async (req, res) => {
      const cityId = parseInt(req.params.id);
      await Widget(req.session.widgets, weatherApi).delete(cityId);
      res.sendStatus(204);
    }
  };

  return { cities, widgets }
};

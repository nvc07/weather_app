build-react:
	@cd front && yarn build

build-docker:
	@docker build -t weather_app .

build: build-react build-docker

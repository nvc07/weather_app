# weather_app

### Running a demo

First create a `back/.env` file with given environment variables:
```
WEATHER_API_KEY=secret-token-from-the-task-description
WEATHER_API_URL=http://localhost:5263/api
COOKIE_SIGNING_KEY=random-key1
COOKIE_VERIFICATION_KEY=random-key2
```

Then:
```
make build
docker run -dp 9299:9299 --name weather_app_demo --env-file=back/.env weather_app
```

The weather app should now run as a docker container. To watch logs simply run `docker logs -f weather_app_demo`.

### Application architecture

The weather app comprises two main parts - a frontend React.js application and a backend Express.js server acting as an intermediary
between a user interface and an external weather API.

A cookie-based session middleware has been used to store a list of user's weather widgets. Though in general a bad practice, it serves
well the purpose of this demo. One might substitute it with another kind of storage (eg. localStorage or a server-side database) quite easily due to a modular nature of the code and a simple dependency injection that
has been used.

A simple in-memory cache has been used to avoid irrelevant queries to  the 3rd party API when demanding a list of cities.

### Tests

To run Express.js app tests `cd back && yarn test`.

### Development

Run Express.js server: `cd back && yarn start`  
Run an external weather API script: `cd back && yarn run weather-api`  
Run React.js app: `cd front && yarn start`  

### Deployment

See `Makefile` contents for available commands.
